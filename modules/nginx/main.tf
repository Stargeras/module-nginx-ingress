resource "helm_release" "ingress" {
  name             = "ingress"
  namespace        = "ingress"
  create_namespace = "true"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      tls_secret          = var.tls_crt != "" ? "${kubernetes_secret.tls-secret[0].metadata[0].namespace}/${kubernetes_secret.tls-secret[0].metadata[0].name}" : ""
      existing_tls_secret = var.existing_tls_secret
      service_type        = var.service_type
      loadbalancer_ip     = var.loadbalancer_ip
      http_nodeport       = var.http_nodeport
      https_nodeport      = var.https_nodeport
    })
  ]
}
