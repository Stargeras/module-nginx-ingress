resource "kubernetes_secret" "tls-secret" {
  count = var.tls_crt != "" ? 1 : 0
  type  = "kubernetes.io/tls"

  metadata {
    name      = "ingress-tls"
    namespace = "kube-system"
  }
  data = {
    "tls.crt" = var.tls_crt
    "tls.key" = var.tls_key
  }
}
