variable "tls_crt" {}
variable "tls_key" {}
variable "existing_tls_secret" {}
variable "service_type" {}
variable "loadbalancer_ip" {}
variable "http_nodeport" {}
variable "https_nodeport" {}
