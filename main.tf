module "nginx" {
  source              = "./modules/nginx"
  tls_crt             = var.tls_crt
  tls_key             = var.tls_key
  existing_tls_secret = var.existing_tls_secret
  service_type        = var.service_type
  loadbalancer_ip     = var.loadbalancer_ip
  http_nodeport       = var.http_nodeport
  https_nodeport      = var.https_nodeport
}