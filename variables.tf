variable "config_path" {
  default = ""
}

variable "kubernetes_auth_host" {
  description = "The hostname (in form of URI) of the Kubernetes API"
  type        = string
  default     = ""
}
variable "kubernetes_auth_cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication"
  type        = string
  default     = ""
}
variable "kubernetes_auth_client_certificate" {
  default     = ""
  description = "PEM-encoded client certificate for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_key" {
  default     = ""
  description = "PEM-encoded client certificate key for TLS authentication"
  type        = string
}
variable "kubernetes_auth_token" {
  default     = ""
  description = "Service account token"
  type        = string
}

variable "service_type" {
  default     = "LoadBalancer"
  description = "Service type of ingress service"
  type        = string
}

variable "loadbalancer_ip" {
  default     = ""
  description = "IP address to utilize if `service_type` is `LoadBalancer`. Leave blank to auto-assign"
  type        = string
}

variable "http_nodeport" {
  default     = ""
  description = "Port to use for http if `service_type` is `NodePort`"
  type        = string
}

variable "https_nodeport" {
  default     = ""
  description = "Port to use for https if `service_type` is `NodePort`"
  type        = string
}

variable "tls_crt" {
  default     = ""
  description = "TLS certificate to secure ingress endpoints"
  type        = string
}

variable "tls_key" {
  default     = ""
  description = "TLS key to secure ingress endpoints"
  type        = string
}

variable "existing_tls_secret" {
  default     = ""
  description = "Existing TLS secret to secure ingress endpoints"
  type        = string
}
