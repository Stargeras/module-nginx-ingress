# Sourcing example
``` terraform
module "nginx-ingress" {
  source                                 = "git::https://gitlab.com/Stargeras/module-nginx-ingress"
  service_type                           = "LoadBalancer"
  kubernetes_auth_host                   = module.kubernetes.kubernetes_auth_host
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  kubernetes_auth_client_certificate     = module.kubernetes.kubernetes_auth_client_certificate
  kubernetes_auth_client_key             = module.kubernetes.kubernetes_auth_client_key
  kubernetes_auth_token                  = module.kubernetes.kubernetes_auth_token
  tls_crt                                = fileexists("${path.root}/tls.crt") ? file("${path.root}/tls.crt") : ""
  tls_key                                = fileexists("${path.root}/tls.key") ? file("${path.root}/tls.key") : ""
}
```
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_nginx"></a> [nginx](#module\_nginx) | ./modules/nginx | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_http_nodeport"></a> [http\_nodeport](#input\_http\_nodeport) | Port to use for http if `service_type` is `NodePort` | `string` | `""` | no |
| <a name="input_https_nodeport"></a> [https\_nodeport](#input\_https\_nodeport) | Port to use for https if `service_type` is `NodePort` | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#input\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#input\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#input\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication | `string` | n/a | yes |
| <a name="input_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#input\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API | `string` | n/a | yes |
| <a name="input_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#input\_kubernetes\_auth\_token) | Service account token | `string` | `""` | no |
| <a name="input_loadbalancer_ip"></a> [loadbalancer\_ip](#input\_loadbalancer\_ip) | IP address to utilize if `service_type` is `LoadBalancer`. Leave blank to auto-assign | `string` | `""` | no |
| <a name="input_service_type"></a> [service\_type](#input\_service\_type) | Service type of ingress service | `string` | `"LoadBalancer"` | no |
| <a name="input_tls_crt"></a> [tls\_crt](#input\_tls\_crt) | TLS certificate to secure ingress endpoints | `string` | `""` | no |
| <a name="input_tls_key"></a> [tls\_key](#input\_tls\_key) | TLS key to secure ingress endpoints | `string` | `""` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->